<header class="banner">
  <div class="header-wrap">
  <div class="container">
    <div class="my-brand" >
      <a href="" class="my-logo" style="background-image:url(<?= get_field('header_image', 11); ?>);"></a>
    </div>
    <div class="header-content">
      <h2><?php the_field('header_title', 11);?></h2>
      <p><?php the_field('header_details', 11);?></p>
    </div>
    <div>
  </div>
</header>
