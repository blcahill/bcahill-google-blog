<footer class="content-info">	  
	<div class="top-footer">
		<div class="container">
			<div class="row adjust">
				<?php while( have_rows('column_content', 11) ): the_row(); ?>
				<div class="col-md-4">
					<h2><?= the_sub_field('list_title', 11); ?></h2>
					<ul>
						<?php while( have_rows('list_items', 11) ): the_row(); ?>
						<li><a href="<?php the_sub_field('item_url', 11);?>"><?php the_sub_field('item_title', 11);?></a></li>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container bottom-footer-wrap">
			<div class="footer-logo-container">
				<a href="" class="footer-logo" style="background-image:url(<?= get_field('footer_image', 11); ?>);"></a>
			</div>
			<div class="actions">
				<ul>
					<?php while( have_rows('bottom_footer_link', 11) ): the_row(); ?>
					<li><a href="<?php the_sub_field('link_url', 11);?>"><?php the_sub_field('link_text', 11);?></a></li>
					<?php endwhile; ?>
				</ul>
			</div>
		</div>
	</div>
</footer>
