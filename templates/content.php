<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_content(); ?>
  </div>
  <div class="mobile-entry-summary">
    <?php the_excerpt(); ?>
  </div>
 <p class="byline author vcard"><?= __('Posted by', 'sage'); ?> <?= get_the_author(); ?></p>
</article>
