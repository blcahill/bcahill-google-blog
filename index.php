
<div class="content-container">
<div class="left-content">
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<div class="pagination">
	<div class="nav-icons">
		<?php  $args=['prev_text'=> '<i class="fa fa-arrow-right fa-2x"></i>', 'next_text'=> '<i class="fa fa-arrow-left fa-2x"></i>'];?>
		<?php the_posts_navigation($args); ?>
	</div>
	<div class="nav">
	<a href="<?php bloginfo('url')?>" ><i class="fa fa-home fa-2x"></i></a>
	</div>
</div>
</div>

<div class="right-content">
<?php dynamic_sidebar('sidebar-primary'); ?>
</div>
	


</div>